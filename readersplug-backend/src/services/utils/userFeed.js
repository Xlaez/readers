const myCustomLabels = require('../../utils/labelPaginate');
const { User, Community } = require('../../models');

/**
 *
 * @param {string} userId is used to check if user is a follower, if
 */
const getMostFollowedUsers = async (userId, { limit, page }) => {
  const options = {
    lean: true,
    customLabels: myCustomLabels,
  };
  const followers = await User.paginate(
    {
      _id: { $ne: userId },
    },
    {
      page,
      sort: { followerCount: -1 },
      ...(limit ? { limit } : { limit: 10 }),
      select: ['name', 'username', 'avatar', 'followerCount', 'followingCount'],
      ...options,
    }
  );
  return followers;
};

const getMostPopulatedCommunities = async (userId, { limit, page }) => {
  const options = {
    lean: true,
    customLabels: myCustomLabels,
  };
  const communities = await Community.paginate(
    {
      'members.id': { $nin: userId },
    },
    {
      page,
      sort: { membersCount: -1 },
      ...(limit ? { limit } : { limit: 10 }),
      select: ['name', 'info', 'type', 'membersCount', 'createdAt'],
      ...options,
    }
  );
  return communities;
};

module.exports = {
  getMostFollowedUsers,
  getMostPopulatedCommunities,
};
