// eslint-disable-next-line no-unused-vars
const myCustomLabels = require('../../utils/labelPaginate');

const { Book, BookComment } = require('../../models');

const createComment = async (bookId, content, userId, parentId) => {
  let newComment = new BookComment({
    author: userId,
    bookId,
    content,
  });

  if (parentId) {
    const pCom = await BookComment.findOne({ _id: parentId });

    if (!pCom) throw new Error('parent comment not found');

    newComment.parentId = parentId;

    await BookComment.updateOne(
      { _id: parentId },
      {
        $inc: { totalReplies: 1 },
        $addToSet: { replies: newComment._id },
      }
    );
  }
  newComment = await newComment.save();

  // eslint-disable-next-line no-return-await
  return await Book.updateOne(
    { _id: bookId },
    {
      $inc: { commentCount: 1 },
    }
  );
};

module.exports = {
  createComment,
};
