const httpStatus = require('http-status');
const { Book } = require('../../models');
const ApiError = require('../../utils/ApiError');

/**
 * Create a book
 * @param {Object} bookBody
 * @returns {Promise<Book>}
 */
const createBook = async (bookBody) => {
  if (await Book.isNameTaken(bookBody.title + bookBody.author)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Book already exists');
  }
  return Book.create(bookBody);
};

/**
 * Query for books
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryBooks = async (filter, options) => {
  const books = await Book.paginate(filter, options);
  return books;
};

/**
 * Get book by id
 * @param {ObjectId} id
 * @returns {Promise<Book>}
 */
const getBookById = async (id) => {
  return Book.findById(id);
};

/**
 * Get book by author
 * @param {string} author
 * @returns {Promise<Book>}
 */
const getBookByAutor = async (author) => {
  return Book.findOne({ author });
};

/**
 * Get book by title
 * @param {string} title
 * @returns {Promise<Book>}
 */
const getBookByTitle = async (title) => {
  return Book.findOne({ title });
};

const getBooksByTitle = async (title) => {
  return Book.find({ title: { $regex: title, $options: 'ig' } });
};

/**
 * Query for books
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */

const getBooksByCategories = async (filter, options) => {
  const books = Book.paginate(filter, options);
  return books;
};

/**
 * Update book by id
 * @param {ObjectId} bookId
 * @param {Object} updateBody
 * @returns {Promise<Book>}
 */
const updateBookById = async (bookId, updateBody) => {
  const book = await getBookById(bookId);
  if (!book) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Book not found');
  }
  if (updateBody.title && (await Book.isNameTaken(updateBody.title + book.author, book.id))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Book already exists');
  }
  Object.assign(book, updateBody);
  await book.save();
  return book;
};

/**
 * Delete book by id
 * @param {ObjectId} bookId
 * @returns {Promise<Book>}
 */
const deleteBookById = async (bookId) => {
  const book = await getBookById(bookId);
  if (!book) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Book not found');
  }
  await book.remove();
  return book;
};

const hasLiked = async (bookId, userId) => {
  const result = await Book.findOne({ $and: [{ _id: bookId }, { likedBy: { $in: userId } }] });
  return result;
};

const hasDisliked = async (bookId, userId) => {
  const result = await Book.findOne({ $and: [{ _id: bookId }, { dislikedBy: { $in: userId } }] });
  return result;
};

const like = async (bookId, userId) => {
  const result = await Book.updateOne({ _id: bookId }, { $inc: { likes: 1 }, $addToSet: { likedBy: userId } });
  return result;
};

const dislike = async (bookId, userId) => {
  const result = await Book.updateOne({ _id: bookId }, { $inc: { dislikes: 1 }, $addToSet: { dislikedBy: userId } });
  return result;
};

const incrementViews = async (bookId) => {
  const result = await Book.updateOne({ _id: bookId }, { $inc: { views: 1 } });
  return result;
};

module.exports = {
  createBook,
  queryBooks,
  getBookById,
  getBookByAutor,
  getBookByTitle,
  updateBookById,
  deleteBookById,
  getBooksByTitle,
  getBooksByCategories,
  like,
  dislike,
  hasDisliked,
  hasLiked,
  incrementViews,
};
