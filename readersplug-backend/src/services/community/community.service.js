// eslint-disable-next-line no-unused-vars
const myCustomLabels = require('../../utils/labelPaginate');

const { Community, CommunityRequest } = require('../../models');

/**
 *
 * @param {object} data is an object that contains the community details to b saved to the database
 */
const saveCommunity = async (data) => {
  const community = await Community.create(data);
  return community;
};

const getACommunityById = async (id) => {
  const community = await Community.findById(id);
  return community;
};

const getCommunityByName = async (name) => {
  const community = await Community.findOne({ name });
  return community;
};

const getMembers = async (id) => {
  const members = await Community.findById(id)
    .populate('members.id', 'avatar name username _id')
    .populate('admins.id', 'avatar name username _id')
    .select(['members', 'admins', 'name'])
    .lean();
  return members;
};

const queryCommunities = async ({ search, filter }, { limit, page, orderBy, sortedBy }) => {
  const options = {
    lean: true,
    customLabels: myCustomLabels,
  };
  const communities = await Community.paginate(
    {
      $or: [{ name: { $regex: search, $options: 'i' } }, { info: { $regex: search, $options: 'i' } }],
      ...filter,
    },
    {
      ...(limit ? { limit } : { limit: 5 }),
      page,
      sort: { [orderBy]: sortedBy === 'asc' ? 1 : -1 },
      ...options,
    }
  );
  return communities;
};

/**
 *
 * @param {string} id is the community's id
 * @param {object} data is the data to be updated
 * @param {object} opts is not required, it holds any options that controller want's to pass
 */

const updateCommunity = async (id, data, opts) => {
  const community = await Community.findByIdAndUpdate(id, data, opts);
  return community;
};

const uploadImage = async (id, url, publicId) => {
  const community = await Community.updateOne({ _id: id }, { coverImage: url, publicId });
  return community;
};

const deleteCommunity = async (id) => {
  const community = await Community.deleteOne({ _id: id });
  return community;
};

// this function creates a new community request for a private group
const sendRequestTo = async (userId, communityId) => {
  const isCommunityPublic = await Community.findOne({ _id: communityId, type: 'public' });

  if (isCommunityPublic) throw new Error("it's needless to send a request to a public ocmmunity");

  const community = await CommunityRequest.requestModel.create({
    userId,
    communityId,
  });
  return community;
};

const deleteRequest = async (id) => {
  // eslint-disable-next-line no-return-await
  return await CommunityRequest.requestModel.deleteOne({ _id: id });
};

const getAllRequests = async ({ id, limit, page }) => {
  const options = {
    lean: true,
    customLabels: myCustomLabels,
  };
  const requests = await CommunityRequest.requestModel.paginate(
    { communityId: id },
    {
      ...(limit ? { limit } : { limit: 15 }),
      page,
      sort: 'asc',
      populate: { path: 'userId', select: 'avatar name username' },
      ...options,
    }
  );
  return requests;
};

module.exports = {
  saveCommunity,
  getACommunityById,
  updateCommunity,
  deleteCommunity,
  queryCommunities,
  getCommunityByName,
  uploadImage,
  getMembers,
  sendRequestTo,
  deleteRequest,
  getAllRequests,
};
