// eslint-disable-next-line no-unused-vars
const myCustomLabels = require('../../utils/labelPaginate');

// remember to remove the comment array from community posts to increase indexing and search

const { CommunityComment, CommunityPost } = require('../../models');

const createComment = async (postId, content, userId, parentId) => {
  let newComment = new CommunityComment({
    author: userId,
    content,
    postId,
  });

  if (parentId) {
    const pCom = await CommunityPost.findOne({ _id: postId, comments: parentId });
    if (!pCom) throw new Error('parent comment not found');

    newComment.parentId = parentId;

    await CommunityComment.findByIdAndUpdate(parentId, {
      $inc: { replyCount: 1 },
      $addToSet: { replies: newComment._id },
    });
  }
  newComment = await newComment.save();

  // eslint-disable-next-line no-return-await
  return await CommunityPost.updateOne(
    { _id: postId },
    {
      $inc: { commentCount: 1 },
      $addToSet: { comments: newComment._id },
    }
  );
};

const queryComments = async ({ postId, limit, page, orderBy, sortedBy }) => {
  const options = {
    lean: true,
    customLabels: myCustomLabels,
  };

  const comments = await CommunityComment.paginate(
    {
      postId,
    },
    {
      ...(limit ? { limit } : { limit: 20 }),
      page,
      sort: { [orderBy]: sortedBy === 'asc' ? 1 : -1 },
      ...options,
    }
  );

  return comments;
};

const updateComment = async (content, id) => {
  const comment = await CommunityComment.updateOne({ _id: id }, { content });
  return comment;
};

const deleteComment = async (commentId) => {
  const comment = await CommunityComment.findById(commentId);

  if (!comment) throw new Error('comment not found');

  const { parentId, postId } = comment;

  const post = await CommunityPost.findById(postId);

  const { comments, commentCount } = post;

  if (!comments.length && commentCount < 1) throw new Error('no commnts');

  if (parentId) {
    await CommunityComment.findByIdAndUpdate(parentId, { $pull: { replies: comment._id }, $inc: { replyCount: -1 } });
  }

  await CommunityPost.findByIdAndUpdate(
    comment.postId,
    { $pull: { comments: commentId } },
    { $inc: { commentCount: -1 } }
  ).lean();

  // eslint-disable-next-line no-return-await
  return await CommunityComment.deleteOne({ _id: commentId });
};

module.exports = {
  createComment,
  queryComments,
  updateComment,
  deleteComment,
};
