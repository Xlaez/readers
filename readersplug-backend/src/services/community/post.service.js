// eslint-disable-next-line no-unused-vars
const myCustomLabels = require('../../utils/labelPaginate');

const { CommunityPost } = require('../../models');

/**
 * @param {object} data
 */
const createPost = async (data) => {
  const post = await CommunityPost.create(data);
  return post;
};

/**
 * @param {string} id
 */
const getPostById = async (id) => {
  const post = await CommunityPost.findById(id).populate('author', 'name username avatar');
  return post;
};

const updatePost = async (data, id) => {
  const post = await CommunityPost.updateOne({ _id: id }, data);
  return post;
};

const deletePost = async (id) => {
  // eslint-disable-next-line no-return-await
  return await CommunityPost.deleteOne({ _id: id });
};

// gets both shares and likes
const getPostLikes = async (postId) => {
  const post = await CommunityPost.findById(postId)
    .populate('likes.likedBy', 'name avatar username')
    .populate('shares.sharedBy', 'name avatar username')
    .select('likes', 'shares')
    .lean();
  return post;
};

const getPosts = async ({ search, filter, communityId }, { limit, page, orderBy, sortedBy }) => {
  const options = {
    lean: true,
    customLabels: myCustomLabels,
  };

  const posts = await CommunityPost.paginate(
    {
      $and: [{ content: { $regex: search, $options: 'i' } }, { communityId }],
      ...filter,
    },
    {
      ...(limit ? { limit } : { limit: 15 }),
      page,
      sort: { [orderBy]: sortedBy === 'asc' ? 1 : -1 },
      ...options,
    }
  );
  return posts;
};

const isPostLikedByUser = async (userId, postId) => {
  const post = await CommunityPost.findOne({ $and: [{ _id: postId }, { 'likes.likedBy': { $in: userId } }] });
  return post;
};

/**
 *
 * @param {string} postId
 * @param {string} userId
 * @param {string} communityId
 * @param {string} data is an optional param which can contain a post file or a post content
 */
const shareAPost = async (postId, userId, communityId, data) => {
  // data to use for creation of the shared post
  const body = { author: userId, communityId, sharedPostId: postId };

  if (data.file) {
    const d = {
      file: {
        url: data.file.url,
        publicId: data.file.publicId,
      },
    };
    Object.assign(body, d);
  } else if (data.content) {
    Object.assign(body, { content: data.content });
  }

  // update this part of the code to transactions in the future
  const post = await createPost(body);

  if (!post) throw new Error('cannot create post');

  // eslint-disable-next-line no-return-await
  return await CommunityPost.updateOne(
    { _id: postId },
    { $addToSet: { 'shares.sharedBy': userId }, $inc: { 'shares.count': 1 } }
  );
};

module.exports = {
  createPost,
  updatePost,
  deletePost,
  getPostById,
  getPosts,
  isPostLikedByUser,
  getPostLikes,
  shareAPost,
};
