const { Note } = require('../models');

/**
 *
 * @param {object} data
 * @returns typeof object
 */
const createNote = async (data) => {
  const note = await Note.create(data);
  return note;
};

module.exports = {
  createNote,
};
