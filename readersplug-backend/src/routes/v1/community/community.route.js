const express = require('express');
const validate = require('../../../middlewares/validate');
const { communityValidation } = require('../../../validations');
const { communityController, communityPostController, communityCommentController } = require('../../../controllers');
const { singleUpload, multipleUpload } = require('../../../libs/multer');
// eslint-disable-next-line no-unused-vars
const auth = require('../../../middlewares/auth');
const validateAcc = require('../../../middlewares/validateUser');

const router = express.Router();

router.post('/new', validateAcc, validate(communityValidation.createCommunity), communityController.createCommunity);
router.get('/', validateAcc, communityController.queryCommunities);
router.get('/by-id/:id', validateAcc, communityController.getCommunityById);
router.get(
  '/post/share/:communityId/:postId',
  validateAcc,
  validate(communityValidation.sharePost),
  communityPostController.sharePost
);
router.post('/comment', validateAcc, communityCommentController.submitComment);
router.get('/comment/:postId', validateAcc, communityCommentController.getComments);

// get request scheduled down to avoid shadowing
router.get('/:name', validateAcc, communityController.getCommunityByName);
router.get('/members/:id', validateAcc, communityController.getMembers);
router.get('/members/request/:id', validateAcc, communityController.getRequests);
router.post('/members/request/:id', validateAcc, communityController.sendRequestToCommunity);
router.delete('/members/request/:id', validateAcc, communityController.rejectRequest);
router.patch('/members/accept-request/:communityId/:requestId/:userId', validateAcc, communityController.acceptRequest);

router.put(
  '/members/add/:id',
  validateAcc,
  validate(communityValidation.joinOrLeaveCommunity),
  communityController.addMember
);
router.purge(
  '/members/remove/:id',
  validateAcc,
  validate(communityValidation.joinOrLeaveCommunity),
  communityController.removeMember
);
router.put('/admins/add/:id', validateAcc, validate(communityValidation.addOrRemoveAdmin), communityController.addAdmin);
router.purge(
  '/admins/remove/:id',
  validateAcc,
  validate(communityValidation.addOrRemoveAdmin),
  communityController.removeAdmin
);
router.patch('/update-info/:id', validateAcc, validate(communityValidation.updateInfo), communityController.updateInfo);
router.patch(
  '/update-other/:id',
  validateAcc,
  validate(communityValidation.updateRulesAndType),
  communityController.updateRulesAndType
);
router.patch('/upload-image/:id', validateAcc, singleUpload, communityController.uploadImage);
router.delete('/:id', validateAcc, communityController.deleteCommunity);

/**
 * FROM HERE INCLUDES ROUTES BELONGING TO COMMUNITY POSTS
 */

router.post(
  '/post',
  validateAcc,
  multipleUpload,
  validate(communityValidation.createPost),
  communityPostController.createPost
);
router.get('/post/by-id/:id', validateAcc, communityPostController.getPostById);
router.get('/post/:communityId', validateAcc, communityPostController.queryPosts);
// this route gets comments total, likes and shares
router.get('/post/likes/:id', validateAcc, communityPostController.getPostLikes);
router.patch('/post/like/:id', validateAcc, communityPostController.likePost);

router.patch('/comment/:commentId', validateAcc, communityCommentController.updateComment);
router.delete('/comment/:commentId', validateAcc, communityCommentController.deleteComment);

router.patch('/post/unlike/:id', validateAcc, communityPostController.unlikePost);
router.patch('/post/:id', validateAcc, validate(communityValidation.updatePost), communityPostController.updatePosts);
router.delete('/post/:id', validateAcc, communityPostController.deletePost);

module.exports = router;
