const express = require('express');
// eslint-disable-next-line no-unused-vars
const auth = require('../../middlewares/auth');
const validateAcc = require('../../middlewares/validateUser');
const { noteController } = require('../../controllers');

const router = express.Router();

router.post('/new', validateAcc, noteController.saveNote);

module.exports = router;
