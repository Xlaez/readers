module.exports.authController = require('./auth.controller');
module.exports.userController = require('./user.controller');
module.exports.convController = require('./conv.controller');
module.exports.bookController = require('./books/book.controller');
module.exports.bookCommentController = require('./books/comment.controller');

module.exports.groupController = require('./group.controller');
module.exports.waitlistController = require('./waitlist.contoller');
module.exports.communityController = require('./community/community.controller');
module.exports.communityPostController = require('./community/post.controller');
module.exports.communityCommentController = require('./community/comment.controller');
module.exports.noteController = require('./note.controller');
module.exports.transactionnController = require('./transaction.controller');
