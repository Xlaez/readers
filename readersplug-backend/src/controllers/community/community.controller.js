const httpStatus = require('http-status');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { communityService } = require('../../services');
const { uploadSingle, deleteSingle } = require('../../libs/cloudinary');

const createCommunity = catchAsync(async (req, res) => {
  const { body } = req;

  const isName = await communityService.getCommunityByName(body.name);

  if (isName) throw new ApiError(httpStatus.BAD_REQUEST, 'name already taken');

  await communityService.saveCommunity({
    ...body,
    admins: { id: [req.user._id] },
    adminCount: 1,
  });
  if (!body) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot create community');

  res.status(201).send('Created');
});

const queryCommunities = catchAsync(async (req, res) => {
  const { search, limit, page, filter, sortedBy, orderBy } = req.query;

  const communities = await communityService.queryCommunities({ search, filter }, { limit, page, orderBy, sortedBy });

  if (!communities) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'resources not found');

  res.status(200).send(communities);
});

const uploadImage = catchAsync(async (req, res) => {
  const { file } = req;

  const { publicId, url } = await uploadSingle(file.path);

  const community = await communityService.uploadImage(req.params.id, url, publicId);

  if (community.modifiedCount !== 1) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'problem uploading cover image');

  res.status(200).send('uploaded');
});

const updateInfo = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.updateCommunity(id, { info: req.body.info });

  if (!community) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot update community info');

  res.status(200).send('updated');
});

const updateRulesAndType = catchAsync(async (req, res) => {
  const { id } = req.params;

  let community;

  if (req.body.rules && req.body.type) {
    community = await communityService.updateCommunity(id, { rules: req.body.rules, type: req.body.type });
  } else if (req.body.rules) {
    community = await communityService.updateCommunity(id, { rules: req.body.rules });
  } else if (req.body.type) {
    community = await communityService.updateCommunity(id, { type: req.body.type });
  }

  if (!community) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot update community info');

  res.status(200).send('updated');
});

const getCommunityByName = catchAsync(async (req, res) => {
  const { name } = req.params;

  const community = await communityService.getCommunityByName(name);

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');

  res.status(200).send(community);
});

const getCommunityById = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.getACommunityById(id);

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');

  res.status(200).send(community);
});

const addMember = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.updateCommunity(id, {
    $addToSet: { members: { id: req.body.member } },
    $inc: { membersCount: 1 },
  });

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');

  res.status(200).send('user added');
});

const addAdmin = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.updateCommunity(id, {
    $addToSet: { admins: { id: req.body.admin } },
    $inc: { adminCount: 1 },
  });

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');
  res.status(200).send('admin added');
});

const removeMember = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.updateCommunity(id, {
    $pull: { members: { id: req.body.member } },
    $inc: { membersCount: -1 },
  });

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');

  res.status(200).send('member removed');
});

const removeAdmin = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.updateCommunity(id, {
    $pull: { admins: { id: req.body.admin } },
    $inc: { adminCount: -1 },
  });

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');
  res.status(200).send('admin removed');
});

const getMembers = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.getMembers(id);

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');

  res.status(200).send(community);
});

const sendRequestToCommunity = catchAsync(async (req, res) => {
  const { id } = req.params;

  const request = await communityService.sendRequestTo(req.user._id, id);

  if (!request) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot send request');

  res.status(201).send('sent');
});

// there is a bug here that prevents deletion of request
const acceptRequest = catchAsync(async (req, res) => {
  const { communityId, requestId, userId } = req.params;

  const community = await communityService.updateCommunity(communityId, {
    $addToSet: { members: { id: userId } },
    $inc: { memberCount: 1 },
  });

  await communityService.deleteRequest(requestId);

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');
  res.status(200).send('member added');
});

const getRequests = catchAsync(async (req, res) => {
  const { id } = req.params;
  const { limit, page } = req.query;

  const requests = await communityService.getAllRequests({ id, limit, page });

  if (!requests) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');

  res.status(200).send(requests);
});

const rejectRequest = catchAsync(async (req, res) => {
  const { id } = req.params;

  const request = await communityService.deleteRequest(id);

  if (request.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'failed');

  res.status(200).send('deleted');
});

const deleteCommunity = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.getACommunityById(id);

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');

  const result = await deleteSingle(community.coverImage.url);

  if (!result) throw new ApiError(httpStatus.EXPECTATION_FAILED, 'could not delte coverImage from cloud');

  await communityService.deleteCommunity(community._id);

  res.status(200).send('deleted');
});

module.exports = {
  createCommunity,
  queryCommunities,
  uploadImage,
  getCommunityByName,
  getCommunityById,
  addMember,
  removeMember,
  getMembers,
  deleteCommunity,
  addAdmin,
  removeAdmin,
  updateInfo,
  updateRulesAndType,
  sendRequestToCommunity,
  acceptRequest,
  getRequests,
  rejectRequest,
};
