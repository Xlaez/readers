const httpStatus = require('http-status');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { communityPostService } = require('../../services');
const { uploadMany, uploadSingle } = require('../../libs/cloudinary');

const createPost = catchAsync(async (req, res) => {
  const { files, body } = req;

  const data = { content: body.content, author: req.user._id, communityId: body.communityId };

  if (files.length) {
    const filePaths = files.map((file) => file.path);
    const result = await uploadMany(filePaths);
    const multipleFiles = result.map((file) => ({ url: file.url, publicId: file.publicId }));

    Object.assign(data, { file: multipleFiles });
  }

  const post = await communityPostService.createPost(data);

  if (!post) throw new ApiError(httpStatus.EXPECTATION_FAILED, 'cannot create post');

  res.status(201).send('created');
});

const queryPosts = catchAsync(async (req, res) => {
  const { search, limit, page, filter, sortedBy, orderBy } = req.query;
  const { communityId } = req.params;

  const posts = await communityPostService.getPosts({ search, filter, communityId }, { limit, page, orderBy, sortedBy });

  if (!posts) throw new ApiError(httpStatus.NOT_FOUND, 'resources not found');

  res.status(200).send(posts);
});

const getPostById = catchAsync(async (req, res) => {
  const post = await communityPostService.getPostById(req.params.id);

  if (!post) throw new ApiError(httpStatus.NOT_FOUND, 'resources not found');

  res.status(200).send(post);
});

const updatePosts = catchAsync(async (req, res) => {
  const post = await communityPostService.updatePost({ content: req.body.content }, req.params.id);

  if (post.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot update post');

  res.status(200).send('updated');
});

const deletePost = catchAsync(async (req, res) => {
  const post = await communityPostService.deletePost(req.params.id);

  if (post.deletedCount !== 1) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot delete post');

  res.status(200).send('deleted');
});

const likePost = catchAsync(async (req, res) => {
  const { user } = req;

  const isPost = await communityPostService.isPostLikedByUser(user._id, req.params.id);

  if (isPost) throw new ApiError(httpStatus.BAD_REQUEST, 'user has liked post already');

  const opts = {
    $addToSet: { 'likes.likedBy': user._id },
    $inc: { 'likes.count': 1 },
  };

  const post = await communityPostService.updatePost(opts, req.params.id);

  if (post.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot like post');

  res.status(200).send('liked!');
});

const unlikePost = catchAsync(async (req, res) => {
  const { user } = req;

  const isPost = await communityPostService.isPostLikedByUser(user._id, req.params.id);

  if (!isPost) throw new ApiError(httpStatus.BAD_REQUEST, "user hasn't liked post");

  const post = await communityPostService.updatePost(
    {
      $pull: { 'likes.likedBy': user._id },
      $inc: { 'likes.count': -1 },
    },
    req.params.id
  );

  if (post.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot unlike post');

  res.status(200).send('unliked!');
});

// this controller gets both likes and shares and in the future would alos get comments
const getPostLikes = catchAsync(async (req, res) => {
  const likes = await communityPostService.getPostLikes(req.params.id);

  if (!likes) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');

  res.status(200).send(likes);
});

const sharePost = catchAsync(async (req, res) => {
  const { user, file, body } = req;
  const { communityId, postId } = req.params;

  let media = {};

  if (file) {
    const { url, publicId } = await uploadSingle(file.path);
    media = {
      file: {
        url,
        publicId,
      },
    };
  }

  const post = await communityPostService.shareAPost(postId, user._id, communityId, { ...body, ...media });

  if (post.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot share post');

  // eslint-disable-next-line no-console
  console.info(post);

  res.status(201).send('shared');
});

module.exports = {
  createPost,
  queryPosts,
  getPostById,
  updatePosts,
  deletePost,
  likePost,
  unlikePost,
  getPostLikes,
  sharePost,
};
