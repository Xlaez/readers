const httpStatus = require('http-status');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { communityComment } = require('../../services');
// eslint-disable-next-line no-unused-vars
const { uploadMany, uploadSingle } = require('../../libs/cloudinary');

/**
 * TODO: implement likes and dsilike for commments in the future
 */

const submitComment = catchAsync(async (req, res) => {
  const { user, body } = req;

  const comment = await communityComment.createComment(body.postId, body.content, user._id, body.parentId || null);

  if (comment.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot comment');

  res.status(200).send('comment made');
});

const getComments = catchAsync(async (req, res) => {
  const { limit, page, sortedBy, orderBy } = req.query;

  const comments = await communityComment.queryComments({ postId: req.params.postId, limit, page, orderBy, sortedBy });

  if (!comments) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');

  res.status(200).send(comments);
});

const updateComment = catchAsync(async (req, res) => {
  const { content } = req.body;
  const { commentId } = req.params;

  if (!content) throw new ApiError(httpStatus.BAD_REQUEST, 'provide content');

  const comment = await communityComment.updateComment(content, commentId);

  if (comment.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot update comment');

  res.status(200).send('updated');
});

const deleteComment = catchAsync(async (req, res) => {
  const { commentId } = req.params;

  const result = await communityComment.deleteComment(commentId);

  if (result.deletedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot delete comment');

  res.status(200).send('deleted');
});

module.exports = {
  submitComment,
  getComments,
  updateComment,
  deleteComment,
};
