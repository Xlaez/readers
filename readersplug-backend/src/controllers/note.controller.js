/* eslint-disable no-unused-vars */
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { noteService } = require('../services');

const saveNote = catchAsync(async (req, res) => {
  // do others
});

module.exports = { saveNote };
