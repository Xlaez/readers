const httpStatus = require('http-status');
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { bookService } = require('../../services');
const { uploadSingle } = require('../../libs/cloudinary');

const createBook = catchAsync(async (req, res) => {
  const { file } = req;
  const data = { ...req.body, author: req.user._id };
  if (file) {
    const { url } = await uploadSingle(file.path);
    data.cover = url;
  }

  const book = await bookService.createBook(data);
  if (!book) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'an error occured');
  res.status(httpStatus.CREATED).send('created');
});

const getBooks = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['author', 'type', 'mature']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await bookService.queryBooks(filter, options);
  res.send(result);
});

const getBookById = catchAsync(async (req, res) => {
  const book = await bookService.getBookById(req.params.bookId);
  if (!book) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Book not found');
  }
  res.send(book);
});

const getBookByTitle = catchAsync(async (req, res) => {
  const book = await bookService.getBookByTitle(req.params.name);

  if (!book) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Book not found');
  }
  res.send(book);
});

const queryBookByTitle = catchAsync(async (req, res) => {
  const books = await bookService.getBooksByTitle(req.params.name);

  if (!books.length) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Book not found');
  }
  res.send(books);
});

const queryBookByCategories = catchAsync(async (req, res) => {
  const filter = {
    category: { $in: req.query.category },
  };
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const books = await bookService.getBooksByCategories(filter, options);

  if (!books) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Books not found');
  }
  res.send(books);
});

const markBookCompleted = catchAsync(async (req, res) => {
  const book = await bookService.updateBookById(req.params.bookId, { completed: true });
  if (!book) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot update book');
  res.status(200).send('successful');
});

const updateBook = catchAsync(async (req, res) => {
  const book = await bookService.updateBookById(req.params.bookId, req.body);
  res.send(book);
});

const deleteBook = catchAsync(async (req, res) => {
  await bookService.deleteBookById(req.params.bookId);
  res.status(httpStatus.NO_CONTENT).send();
});

const updateProfile = catchAsync(async (req, res, next) => {
  const book = await bookService.updateBookById(req.book.id, req.body);

  if (!book) return next(new ApiError(httpStatus.NOT_FOUND, 'book not found'));
  res.send('updated');
});

const likedBook = catchAsync(async (req, res, next) => {
  const book = await bookService.hasLiked(req.params.bookId, req.user._id);

  if (book) return next(new ApiError(httpStatus.BAD_REQUEST, 'already liked'));

  const result = await bookService.like(req.params.bookId, req.user._id);

  if (result.modifiedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannnot like'));

  res.status(200).send('liiked');
});

const dislikeBook = catchAsync(async (req, res, next) => {
  const book = await bookService.hasDisliked(req.params.bookId, req.user._id);

  if (book) return next(new ApiError(httpStatus.BAD_REQUEST, 'already liked'));

  const result = await bookService.dislike(req.params.bookId, req.user._id);

  if (result.modifiedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannnot like'));

  res.status(200).send('disliked');
});

const viewBook = catchAsync(async (req, res, next) => {
  const result = await bookService.incrementViews(req.params.bookId);

  if (result.matchedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot increment view'));

  res.status(200).send('book views incremented');
});

module.exports = {
  createBook,
  getBooks,
  getBookById,
  updateBook,
  deleteBook,
  updateProfile,
  getBookByTitle,
  queryBookByTitle,
  queryBookByCategories,
  markBookCompleted,
  likedBook,
  viewBook,
  dislikeBook,
};
