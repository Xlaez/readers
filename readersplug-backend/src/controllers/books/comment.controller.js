const httpStatus = require('http-status');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { bookCommentService } = require('../../services');

const postComment = catchAsync(async (req, res, next) => {
  const comment = await bookCommentService.createComment(req.body.bookId, req.body.content, req.user._id, req.body.parentId);

  if (!comment) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot post comment'));

  res.status(200).send(comment);
});

module.exports = {
  postComment,
};
