require("dotenv").config();
const app = require("./app");
const connectDB = require("./database/index");

const port = process.env.PORT || 3000;

const startServer = () => {
  try {
    connectDB(process.env.MONGO_URI);
    app.listen(port, () => {
      console.log(`server runing on port: ${port}`);
    });
  } catch (err) {}
};

startServer();
