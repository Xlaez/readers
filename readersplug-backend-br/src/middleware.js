const { verify } = require('jsonwebtoken');
const User = require('./models/user');

const errorHandler = (err, req, res, next) => {
	console.log(err);
	return res
		.status(500)
		.json({ msg: 'something went wrong.. please try again' });
};

const notFound = (req, res) => {
	res.status(404).json({ msg: 'routes not available...' });
};

const decryptData = (token) => {
	return verify(token, process.env.JWT_SECRET);
};

const authenticate = async (req, res, next) => {
	const token = req.headers['x-auth-token'];

	if (!token) return res.status(403).send('provide a valid token header');

	if (typeof token !== 'string') {
		return res.status(403).send('provide a valid token type');
	}
	try {
		const decode = decryptData(token);

		const user = await User.findById(decode.id).select('-password');

		if (!user) {
			return res.status(403).json('user is not allowed');
		}

		// add the decrypted user's object to the req object
		req.user = user;
		next();
	} catch (e) {
		const errors = ['TokenExpiredError', 'NotBeforeError', 'JsonWebTokenError'];
		if (errors.includes(e?.name)) {
			return res.status(403).json('please authenticate');
		}
		next(e);
	}
};

module.exports = {
	errorHandler,
	notFound,
	authenticate,
};
