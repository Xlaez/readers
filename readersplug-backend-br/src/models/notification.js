const { Schema, model } = require('mongoose');

const schema = new Schema(
	{
		image: {
			type: String,
			enum: ['thumb', 'person'], //this  would be updated  gradually
		},
		message: {
			type: String,
			required: true,
			minLength: 1,
		},
		link: {
			type: String,
		},
		userId: {
			type: Schema.Types.ObjectId,
			ref: 'User',
			required: true,
		},
		isSeen: {
			type: Boolean,
			default: false,
		},
	},
	{
		timestamps: true,
		toJSON: {
			virtuals: true,
		},
	}
);

schema.methods.seen = function () {
	this.isSeen = true;
	return this.save;
};

const notificationSchema = model('Notification', schema);
module.exports = notificationSchema;
