const mongoose = require('mongoose');

const followRequest = new mongoose.Schema(
	{
		id: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: 'User',
		},
	},
	{
		timestamps: true,
		_id: false,
		id: false,
	}
);

const userSchema = new mongoose.Schema(
	{
		avatar: {
			url: {
				type: String,
			},
			publicId: {
				type: String,
			},
		},
		fullName: {
			type: String,
		},
		email: {
			type: String,
			required: true,
		},
		bio: {
			type: String,
			minLenght: 100,
		},
		dob: {
			type: Date,
		},
		location: {
			type: String,
		},
		gender: {
			type: String,
			minLenght: 1,
		},
		password: {
			type: String,
			required: true,
		},
		phoneNumber: {
			type: String,
		},
		username: {
			type: String,
		},
		reader: {
			type: Boolean,
			default: false,
		},
		writer: {
			type: Boolean,
			default: false,
		},
		followRequest: [followRequest], // this is where potential followers get stored until their request is accepted
		unAcceptedRequest: [followRequest], // this array contains request sent to others that haven't been accepted
		followers: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'User',
			},
		],
		following: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'User',
			},
		],
		books: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Book',
			},
		],
		followerCount: {
			type: Number,
			default: 0,
		},
		followingCount: {
			type: Number,
			default: 0,
		},
		bookCount: {
			type: Number,
			default: 0,
		},
	},
	{
		timestamps: true,
	}
);

userSchema.methods.toJSON = function () {
	const user = this;

	const userOBJ = user.toObject();
	delete userOBJ.password;

	return userOBJ;
};

userSchema.method('readerProfile', function () {
	return `Hey ${this.email} welcome. you are a reader`;
});

userSchema.method('writerProfile', function () {
	return `Hey ${this.email} welcome you are a writer`;
});

const User = mongoose.model('User', userSchema);
module.exports = User;
