const { Schema, model } = require('mongoose');

const bookSchema = new Schema(
	{
		userId: {
			type: Schema.Types.ObjectId,
			ref: 'User',
		},
		title: {
			type: String,
			required: true,
			minLength: 1,
		},
		content: {
			type: String,
			required: true,
			minLength: 20,
		},
		coverImage: {
			url: String,
			publicId: String,
		},
		price: {
			type: Number,
			default: 0,
		},
		serviceFee: {
			type: Number,
			default: 0,
		},
		isbn: {
			type: String,
		},
		tags: {
			type: String,
		},
		category: {
			type: String,
			enum: [
				'adventure',
				'fiction',
				'novel',
				'science fiction',
				'mystery',
				'fantasy',
				'history',
				'non fiction',
				'romance',
				'thriller',
				'literature',
				'food',
				'action fiction',
				'drama',
				'graphics novel',
				'self help',
				'autobiography',
				'comic',
				'technology',
				'religion',
				'articles',
				'fairy tale',
				'crime thriller',
				'methodology',
				'textbooks',
				'design',
				'arts',
				'politics',
				'travel',
				'narrative',
			],
		},
		matureContent: {
			type: Boolean,
			default: false,
		},
		completed: {
			type: Boolean,
			default: false,
		},
	},
	{
		timestamps: true,
	}
);

const Book = model('Book', bookSchema);
module.exports = Book;
