const Joi = require('joi');

const validationSchema = Joi.object({
	email: Joi.string()
		.email({
			minDomainSegments: 2,
			tlds: { allow: ['com', 'net'] },
		})
		.required(),
	password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).required(),
	username: Joi.string().alphanum().min(3).max(30),
	reader: Joi.boolean().default(false),
	writer: Joi.boolean().default(false),
});

const updateSchema = async (data) => {
	const schema = Joi.object({
		username: Joi.string().min(2).max(20),
		phoneNumber: Joi.string().min(8).max(13),
		gender: Joi.string(),
		location: Joi.string(),
		fullName: Joi.string(),
		bio: Joi.string().min(101).max(220),
		dob: Joi.date(),
	});
	return schema.validate(data);
};

module.exports = { updateSchema, validationSchema };
