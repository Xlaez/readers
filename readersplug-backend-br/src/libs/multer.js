const multer = require('multer');

class MulterOptions {
	constructor() {}

	fileStorage = multer.diskStorage({});
}

const multerOptions = new MulterOptions();

const singleUpload = multer({
	storage: multerOptions.fileStorage,

	fileFilter: multerOptions.fileFilter,
}).single('upload');

const multipleUploads = multer({
	storage: multerOptions.fileStorage,

	fileFilter: multerOptions.fileFilter,
}).array('upload', 5);

module.exports = {
	singleUpload,
	multipleUploads,
};
