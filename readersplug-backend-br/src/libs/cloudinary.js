const cloudinary = require('cloudinary');
const dotenv = require('dotenv');
const path = require('path');

dotenv.config({ path: path.resolve(process.cwd(), '.env') });
const cloudinaryConfig = cloudinary.v2;

cloudinaryConfig.config({
	cloud_name: process.env.CLOUD_NAME,
	api_key: process.env.CLOUDINARY_API_KEY,
	api_secret: process.env.CLOUDINARY_API_SECRET,
});

const uploadToCloud = async function uploadToCloud(path) {
	const { secure_url, public_id } = await cloudinaryConfig.uploader.upload(
		path
	);
	return { url: secure_url, publicId: public_id };
};

const deleteFromCloud = async function deleteFromCloud(url) {
	return await cloudinaryConfig.uploader.destroy(url);
};

module.exports = {
	uploadToCloud,
	deleteFromCloud,
};
