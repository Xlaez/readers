/** @format */

const express = require('express');
const router = express.Router();

const registerAPI = require('../api/register');
const loginAPI = require('../api/login');
const waitlistAPI = require('../api/waitlis');
const userAPI = require('../api/user');
const bookAPI = require('../api/book');

router.use('/user', registerAPI);
router.use('/user', loginAPI);
router.use('/user', userAPI);
router.use('/book', bookAPI);
router.use(waitlistAPI);

module.exports = router;
