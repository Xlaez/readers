require("dotenv").config();
const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const router = express.Router();

const User = require("../models/user");

router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    //check if user exists
    const checkUser = await User.findOne({ email });
    if (checkUser) {
      const comparePassword = bcrypt.compareSync(password, checkUser.password);
      if (comparePassword) {
        const jwtToken = jwt.sign(
          {
            id: checkUser.id,
            email: checkUser.email,
          },
          process.env.JWT_SECRET,
          {
            expiresIn: "20h",
          }
        );
        res
          .status(200)
          .send({ msg: `welcome back, ${checkUser.email},`, token: jwtToken });
      } else {
        return res.status(400).send({ msg: "email or password is invalid" });
      }
    } else {
      return res.status(400).send({ msg: "email or password is invalid" });
    }
  } catch (err) {
    if (err) return res.status(500).send({ msg: err.message });
  }
});

module.exports = router;
