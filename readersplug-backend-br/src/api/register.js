require('dotenv').config();
const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const router = express.Router();

const salt = bcrypt.genSaltSync(10);

const User = require('../models/user');
const { validationSchema } = require('../helper/userValidation');

router.post('/register', async (req, res) => {
	try {
		const { email, password, reader, writer } = req.body;

		const checkUser = await User.findOne({ email });

		if (checkUser) {
			return res
				.status(400)
				.send({ msg: 'user already exists.. please login' });
		}

		const userValue = await validationSchema.validateAsync({
			email,
			password,
			reader,
			writer,
		});

		const hashPassword = bcrypt.hashSync(password, salt);

		userValue.password = hashPassword;

		const newUser = await User.create(userValue);

		const jwtToken = jwt.sign(
			{
				id: newUser.id,
				email: newUser.email,
			},
			process.env.JWT_SECRET,
			{
				expiresIn: '20h',
			}
		);

		// if user is a reader
		if (newUser.reader) {
			return res.status(200).send({ msg: newUser.readerProfile() });
		}

		// if user is a writer
		if (newUser.writer) {
			return res.status(200).send({ msg: newUser.writerProfile() });
		}

		res.status(201).send({
			user: newUser,
			token: jwtToken,
		});
	} catch (err) {
		if (err) {
			return res.status(500).json({ msg: err.message });
		}
	}
});

module.exports = router;
