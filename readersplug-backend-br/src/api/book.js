const express = require('express');
const { authenticate } = require('../middleware');

const router = express.Router();

const Book = require('../models/books');

/**
 * the kewyord query is an optional field  inwhich is passed only if user seeks books with matching for a name
 * the userId is also optional but passed to if the query is for books by a particular writer
 */

router.get('/all', authenticate, async (req, res) => {
	const { pageNo, pageSize, keyword, userId } = req.query;

	// for pagination
	const pagesToSkip = (+pageNo || 1 - 1) * +pageSize || 12;

	let queryObj = {};

	if (keyword && userId) {
		queryObj = {
			$and: [{ userId }, { title: { $regex: keyword, options: '$i' } }],
		};
	} else if (userId) {
		queryObj = { userId };
	} else if (keyword) {
		queryObj = { title: keyword };
	}

	try {
		const books = await Book.find(queryObj)
			.skip(pagesToSkip)
			.limit(pageSize)
			.sort({ title: 'asc' });

		if (!books?.length)
			return res.status(404).json({ msg: 'resources not found' });

		const count = await Book.find(queryObj).countDocuments();

		res.status(200).json({ books, count });
	} catch (e) {
		res.status(500).json({ msg: e.message });
	}
});

module.exports = router;
