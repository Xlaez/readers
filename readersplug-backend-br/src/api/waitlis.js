/** @format */

const express = require('express');
const Joi = require('joi');
const router = express.Router();

const Waitlist = require('../models/waitlist');

const emailValidation = Joi.object({
	email: Joi.string().email({
		minDomainSegments: 2,
		tlds: { allow: ['com', 'net'] },
	}),
});

router.post('/waitlist', async (req, res, next) => {
	try {
		const { email } = req.body;

		const findExistingemail = await Waitlist.findOne({ email });

		if (findExistingemail) {
			return res.status(400).send({ message: 'you already join the waitlist' });
		}

		const emailWaitlist = await emailValidation.validateAsync({ email });

		const emails = await Waitlist.create(emailWaitlist);

		await emails.save();

		res.status(200).send({
			msg: 'Thanks for Joing the waitlist, we will notify you once we lauch 🤞',
		});
	} catch (err) {
		if (err) {
			res.status(500).json({ msg: err.message });
		}
	}
});

module.exports = router;
