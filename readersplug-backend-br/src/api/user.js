const express = require('express');
const { updateSchema } = require('../helper/userValidation');
const { uploadToCloud } = require('../libs/cloudinary');

const { singleUpload } = require('../libs/multer');
const { authenticate } = require('../middleware');

const router = express.Router();

const User = require('../models/user');

// upload user avatar
router.post('/upload/avatar', singleUpload, authenticate, async (req, res) => {
	const { file, user } = req;

	if (!file) return res.status(400).json({ msg: 'provide a file' });

	try {
		const { url, publicId } = await uploadToCloud(file.path);

		user.avatar = {
			url,
			publicId,
		};
		const updatedUser = await user.save();

		if (!updatedUser) return res.status(500).json('something went wrong');

		res.status(200).json({ msg: 'upload successful' });
	} catch (e) {
		res.status(500).json({ msg: e.mesage });
	}
});

// get users
router.get('/', authenticate, async (req, res) => {
	const { pageNo, pageSize, keyword } = req.query;

	// for pagination
	const pagesToSkip = (+pageNo - 1) * +pageSize;

	let queryObj = {};

	if (keyword) {
		queryObj = { username: { $regex: keyword, $options: '$i' } };
	}

	try {
		const users = await User.find(queryObj)
			.skip(pagesToSkip)
			.limit(pageSize)
			.sort({ username: 'asc' })
			.select(['-password'])
			.lean();

		if (!users?.length) return res.status(404).json('resources not found');

		const count = await User.find(queryObj).countDocuments();

		res.status(200).json({ users, count });
	} catch (e) {
		res.status(500).json(e.message);
	}
});

// get unaccepted sent requests
router.get('/sent-requests', authenticate, async (req, res) => {
	const { user } = req;
	const { pageNo, pageSize } = req.query;

	const no = +pageNo || 1;
	const size = +pageSize || 12;
	const pagesToSkip = (no - 1) * size;

	try {
		const requests = await User.findById(user._id)
			.populate('unAcceptedRequest.id', 'fullName username _id avatar')
			.skip(pagesToSkip)
			.limit(pageSize)
			.select('unAcceptedRequest');

		res.status(200).json(requests);
	} catch (e) {
		res.status(500).json(e.message);
	}
});

// get followers
router.get('/followers', authenticate, async (req, res) => {
	const { user } = req;
	const { pageNo, pageSize } = req.query;

	const no = +pageNo || 1;
	const size = +pageSize || 12;
	const pagesToSkip = (no - 1) * size;

	try {
		const followers = await User.findById(user._id)
			.populate('followers', 'fullName username _id avatar')
			.skip(pagesToSkip)
			.limit(pageSize)
			.select('followers');

		res.status(200).json(followers);
	} catch (e) {
		res.status(500).json(e.message);
	}
});

router.get('/followings', authenticate, async (req, res) => {
	const { user } = req;
	const { pageNo, pageSize } = req.query;

	const no = +pageNo || 1;
	const size = +pageSize || 12;
	const pagesToSkip = (no - 1) * size;

	try {
		const followings = await User.findById(user._id)
			.populate('following', 'fullName username _id avatar')
			.skip(pagesToSkip)
			.limit(pageSize)
			.select('following');

		res.status(200).json(followings);
	} catch (e) {
		res.status(500).json(e.message);
	}
});

// get a user
router.get('/:userId', authenticate, async (req, res) => {
	try {
		const user = await User.findById(req.params.userId).select([
			'-password',
			'-following',
			'-followers',
			'-books',
		]);

		if (!user) return res.status(404).json({ msg: 'user not found' });

		res.status(200).json(user);
	} catch (e) {
		res.status(500).json({ msg: e.message });
	}
});

// update user profile
router.patch('/update', authenticate, async (req, res) => {
	const { user, body } = req;

	try {
		const { error } = await updateSchema(body);

		if (error) return res.status(400).json(error.mesage);

		const u = await User.findByIdAndUpdate(user._id, body);

		if (!u) return res.status(500).json('could not update user');

		res.status(200).json('updated');
	} catch (e) {
		res.status(500).json(e.message);
	}
});

// request to follow user
/**
 * would be updated to make use of transaction like in SQL
 */
router.put('/follow/:userId', authenticate, async (req, res) => {
	const { userId } = req.params;

	try {
		// add current user making request to the other user's followRequest
		const user = await User.findByIdAndUpdate(userId, {
			$addToSet: { followRequest: { id: req.user._id } },
		});

		if (!user) return res.status(500).json('cannot update user');

		// adding the to be followed user to unAcceptedRequest of current user making request
		await User.findByIdAndUpdate(req.user._id, {
			$addToSet: { unAcceptedRequest: { id: userId } },
		});

		res.status(200).json('request sent!');
	} catch (e) {
		res.status(500).json(e?.message);
	}
});

// cancel a sent request
/**
 * would be updated to using transactions as used in SQL
 */
router.purge('/cancel-request/:userId', authenticate, async (req, res) => {
	const { userId } = req.params;
	try {
		// remove current user making request from the other user's followRequest
		await User.findByIdAndUpdate(userId, {
			$pull: { followRequest: { id: req.user._id } },
		});

		// remove the user from unAcceptedRequest of current user making request
		await User.findByIdAndUpdate(req.user._id, {
			$pull: { unAcceptedRequest: { id: userId } },
		});

		res.status(200).json('cancelled request successfully');
	} catch (e) {
		res.status(500).json(e?.message);
	}
});

// accept follow request
/**
 * would implement using transactions later
 */
router.put('/accept-request/:id', authenticate, async (req, res) => {
	const { user } = req;
	const { id } = req.params;

	try {
		// remove user from request to followers and increment follower count
		await User.findByIdAndUpdate(user._id, {
			$pull: { followRequest: { id } },
			$push: { followers: id },
			$inc: { followerCount: 1 },
		});

		// change use from unAccepted to following and increment following
		await User.findByIdAndUpdate(id, {
			$pull: { unAcceptedRequest: { id: user._id } },
			$push: { following: user._id },
			$inc: { followingCount: 1 },
		});

		res.status(200).json('request accepted');
	} catch (e) {
		res.status(500).json(e.message);
	}
});

// unfollow a user
/**
 * would use transactions later in the future
 */
router.purge('/unfollow/:id', authenticate, async (req, res) => {
	const { user } = req;
	const { id } = req.params;

	try {
		await User.findByIdAndUpdate(id, {
			$pull: { followers: user._id },
			$inc: { followerCount: -1 },
		});

		await User.findByIdAndUpdate(user._id, {
			$pull: { following: id },
			$inc: { followingCount: -1 },
		});

		res.status(200).json('unfollow successful');
	} catch (e) {
		res.status(500).json(e.message);
	}
});
module.exports = router;
