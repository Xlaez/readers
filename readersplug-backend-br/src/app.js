const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const morgan = require('morgan');
require('./auth/passport');

require('dotenv').config();
const app = express();

const middleware = require('./middleware');

// middleware
app.use(morgan('dev'));
app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
	res.status(200).send({ msg: "Welcome to reader's plug" });
});

// router handler
app.use('/api/v1', require('./api/index'));

// Error Handler
app.use(middleware.errorHandler);
app.use(middleware.notFound);

module.exports = app;
