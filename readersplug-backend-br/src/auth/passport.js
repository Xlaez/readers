require("dotenv").config();
const passport = require("passport");
const passportJWT = require("passport-jwt");
const User = require("../models/user");
const strategyJWT = passportJWT.Strategy;
const extractJWT = passportJWT.ExtractJwt;

passport.use(
  new strategyJWT(
    {
      jwtFromRequest: extractJWT.fromAuthHeaderAsBearerToken,
      secretOrKey: process.env.JWT_SECRET,
    },
    async (jwtPayload, done) => {
      try {
        const user = await User.findOne({ id: jwtPayload.id });
        return done(null, user);
      } catch (err) {
        return done(err);
      }
    }
  )
);
